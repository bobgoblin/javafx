package sample;

import javafx.scene.control.*;
import java.net.URL;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import java.io.IOException;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class KeywordGrabber extends AnchorPane implements Initializable {

    private static final String DEFAULT_LANG = "en";
    private boolean bProgress = false;
    private KWTool kwTool;
    private Main application;

    @FXML
    TextField txtKeywordSource;
    @FXML
    org.controlsfx.control.CheckListView txtKeywordOutput;
    @FXML
    Button btnProcess;
    @FXML
    Label lblStatus;

    public void setApp(Main application) { this.application = application; }

    public void main(String args[]) { }

    @FXML
    private void GetResults(ActionEvent e) {
        kwTool = new KWTool();
        String _inheritanceKws = "";

        if (btnProcess.getText().equals("Get Keywords")){
            String _keyword = txtKeywordSource.getText();
            if (_keyword != null && !_keyword.equals("")){
                String[] tmpSplit = _keyword.split("\n");
                if (tmpSplit.length > 1){ //. assumed last search
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Single Keyword");
                    alert.setHeaderText("Information");
                    alert.setContentText("Please input single keyword.");
                    alert.show();
                    return;
                }else{
                    boolean err1 = false;
                    bProgress = true;
                    btnProcess.setText("Stop Keyword");
                    //txtKeywordOutput.clear();

                    ArrayList<String> lstKwLevel1 = null;
                    try {
                        lstKwLevel1 = kwTool.fetchKeywords(_keyword, DEFAULT_LANG);
                    } catch (ParserConfigurationException ex) {
                        Logger.getLogger(KeywordGrabber.class.getName()).log(Level.SEVERE, null, ex);
                        err1 = true;
                    } catch (SAXException ex) {
                        Logger.getLogger(KeywordGrabber.class.getName()).log(Level.SEVERE, null, ex);
                        err1 = true;
                    } catch (IOException ex) {
                        Logger.getLogger(KeywordGrabber.class.getName()).log(Level.SEVERE, null, ex);
                        err1 = true;
                    }

                    TextField txtPref = new TextField();

                    if (lstKwLevel1 != null && !err1){
                        if (lstKwLevel1.size() > 0){
                            String mKwLvl1 = "";
                            for (String lvl1 : lstKwLevel1){
                                mKwLvl1 += lvl1.trim() + "\n";
                            }
                            _inheritanceKws = mKwLvl1;

                            lblStatus.setText("Fetch first level keyword..");
                            ArrayList<String> lstKw = kwTool.getListKeyword(txtPref.getText(), _inheritanceKws); //. note-hirin
                            if (lstKw.size() > 0){
                                String mKw = "";
                                for (String szKw : lstKw){
                                    mKw += szKw.trim() + "\n";
                                }
                                _inheritanceKws = mKw;
                            }

                            for (int i = 0; i < lstKw.size() ; i++){
                                String _curKw = lstKw.get(i);
                                if (_curKw != null && !_curKw.isEmpty()){
                                    lblStatus.setText("Process keyword [" + _curKw +  "]");
                                }

                                ArrayList<String> lstKwResult = new ArrayList<>();
                                try {
                                    lstKwResult = kwTool.fetchKeywords(_curKw, DEFAULT_LANG);
                                } catch (ParserConfigurationException ex) {
                                    Logger.getLogger(KeywordGrabber.class.getName()).log(Level.SEVERE, null, ex);
                                } catch (SAXException ex) {
                                    Logger.getLogger(KeywordGrabber.class.getName()).log(Level.SEVERE, null, ex);
                                } catch (IOException ex) {
                                    Logger.getLogger(KeywordGrabber.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                for(String _kwRes : lstKwResult){
                                    //String _newOutText = txtKeywordOutput.getText();
                                    //_newOutText +=_kwRes + "\n";
                                    //txtKeywordOutput.setText(_newOutText); //.
                                }
                            }
                        }else{
                            lblStatus.setText("Fetch first level keyword empty..");
                            Alert alert = new Alert(Alert.AlertType.WARNING);
                            alert.setTitle("Inheritance of keywords is empty.");
                            alert.setHeaderText("Warning");
                            alert.setContentText("Please change your keyword..");
                            alert.show();
                        }

                    }else{
                        lblStatus.setText("Fetch first level keyword error..");
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error Happen");
                        alert.setHeaderText("Error");
                        alert.setContentText("Error happen when fecth first level keyword");
                        alert.show();
                    }


                }
            }else{
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Input Keyword");
                alert.setHeaderText("Warning");
                alert.setContentText("Please input your keyword..");
                alert.show();
            }
            bProgress = false;
            btnProcess.setText("Get Keywords");
            //Alert alert = new Alert(Alert.AlertType.INFORMATION);
            //alert.setTitle("Done");
            //alert.setHeaderText("Information");
            //alert.setContentText("Process done.");
            //alert.show();
        }else{
            bProgress = false;
            btnProcess.setText("Get Keywords");
        }
    }

    @FXML
    private void GoHome(ActionEvent e) {
        //System.out.println("selected text:" + txtKeywordOutput.getSelectedText());
        //application.gotoMain();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}